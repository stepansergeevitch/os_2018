#include<string>
#include<iostream>
#include<unistd.h>
#include<thread>
#include<chrono>
#include<mutex>
#include <condition_variable>
using namespace std;

mutex res_mutex;
bool result = true;

mutex msg_m;
condition_variable msg_cv;

void thread_search(string x, char c)
{
	res_mutex.lock();
	result = result && (x.find(c) != -1);
	res_mutex.unlock();
	lock_guard<mutex> lk(msg_m);
	msg_cv.notify_one();
}

void thread_search_t(string x, char c)
{
	res_mutex.lock();
	result = result && (x.find(c) != -1);
	res_mutex.unlock();
	while(true) {
		usleep(100000);
	}
	lock_guard<mutex> lk(msg_m);
	msg_cv.notify_one();
}


int main()
{
	string x;
	cout << "Enter x: ";
	cin >> x;

	bool res0;
	bool res1;

	thread t0(&thread_search, x, 'a');
	thread t1(&thread_search_t, x, 'b');

    unique_lock<mutex> lk(msg_m);
    msg_cv.wait(lk, []{return true;});

	if (result != false) {
		t0.join();
		t1.join();
		cout << "Result: " << (result ? "True" : "False") << endl;
	} else {
		t0.detach();
		t1.detach();
		t0.~thread();
		t1.~thread();
		cout << "Result: " << (result ? "True" : "False") << endl;
	}
	return 0;
}