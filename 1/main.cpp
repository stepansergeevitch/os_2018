#include<iostream>
#include<vector>
#include<fstream>
#include<string.h>
#include<iostream>
#include<map>

using namespace std;

vector<string> split(string input, char separator = ' ') {
	vector<string> res;
	string buffer;
	for (auto ch: input) {
		if (ch == separator){
			res.push_back(buffer);
			buffer = "";
		} else {
			buffer += ch;
		}
	}
	if (buffer != "") 
		res.push_back(buffer);
	return res;
}	

int main(){
	string words_file_name = "words.txt";
	string text_file_name = "text.txt";

	ifstream words_file(words_file_name);
	string word_buf;
	vector<string> words;
	while (words_file >> word_buf) {
		words.push_back(word_buf);
	}

	ifstream text_file(text_file_name);
	string text(istreambuf_iterator<char>(text_file), {});
	vector<string> text_words = split(text);

	map<string, int> counts;
	for (auto word: text_words) {
			counts[word] += 1;
	}
	for (auto word: words) {
		int count = 0;
		if (counts.find(word) != counts.end())
			count = counts[word];
		cout << word << ": " << count << '\n';
	}
}